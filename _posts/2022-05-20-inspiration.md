---
title: "Inspiration"
layout: post
date: 2022-05-20 12:00
image: /assets/images/markdown.jpg
headerImage: false
tag:
- poems
star: true
category: blog
author: reedbender
description: My ass in the dirt
---


<hr>

My ass in the dirt, my head on a tree

I’m watching a dance as you become me

—

Inhale, I long for more

A hopeless insecurity

Exhale, I let it go

An infinite tranquility

—

No label or judgement, no thought in my skull

I wish I could stay here where my heart is full

—

Inhale, I look outside

An empty compulsivity

Exhale, I feel the center

A boundless infinity

—

My God what a joke, my wish is its loss

I’m back on my own and bearing a cross

—

Inhale, I hope to be there again

A miserable finality

Exhale, I already am

A blissful unicity

—

I pause and sit still, no aim to achieve

A void that can fill passes through as I breathe

—

Inhale, I search

A misguided rationality

Exhale, I return

An endless reciprocity

—

To live with no preference and allow every death

A final surrender with each passing breath

<br/>

*Namaste* :pray:

~ Reed

<hr>

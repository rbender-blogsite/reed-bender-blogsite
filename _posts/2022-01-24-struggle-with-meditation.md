---
title: "I Struggle With Meditation"
layout: post
date: 2022-01-24 12:00
tag:
- meditation
star: true
category: blog
author: reedbender
description: Here's why I still do it
---

<p style="text-align: center;"><i>Here’s why I still do it</i></p>


![image](https://cdn-images-1.medium.com/max/2000/1*YtSmXFzFU2Y8PbGQNsch4g.jpeg)
The Sirens and Ulysses, William Etty (1837)
<br/>

<hr>

> “Your next encounter will be with the Sirens, who bewitch everybody who approaches them. There is no homecoming for the man who draws near them unawares and hears the Sirens’ voices … For with their high clear song the Sirens bewitch him, as they sit there in a meadow piled high with the moldering skeletons of men.”
> — Homer, The Odyssey

My experience with meditation is best characterized by a scene from Homer’s epic poem, *The Odyssey*.

After emerging from the underworld, Ulysses is told he must sail past an island inhabited by creatures who sing a divine song. He is warned against allowing himself or his crew to listen to as the ship passes. In an unconscious act of pure desire, any crew member who pays attention to the song of the Sirens would compulsively steer the ship directly towards the sound. As the captain of the ship, he orders his men to plug their ears. He alone can listen, but he orders that they incapacitate him by tying him to the mast of his ship.

> “You must bind me very tight, standing me up against the step of the mast and lashed to the mast itself so that I cannot stir from the spot. And if I beg and command you to release me, you must tighten and add to my bonds.”

As they pass the song of the Sirens, Ulysses is so impassioned that he tries to free himself and steer towards the music. His crew, however, tightens his bonds. Once they are safely beyond the range of hearing, he returns to his senses and his crew sails on.

When I try and meditate, I feel like Ulysses kicking and screaming against the mast of his ship. I tie myself down, only to resist when the enchanting sounds arise. An endless stream of thoughts floods through my mind, each demanding that I divert course and give them my attention.

*“I am hungry. A muffin sounds nice.”*

*“I must look so stupid sitting here right now.”*

*“What good do I think this will do?”*

*“I’d rather be watching Friends.”*

I listen to these thoughts drone on, surrendering my awareness to them. These thought loops form deep grooves, keeping me trapped in the same mental patterns day after day. The more I attend to any given thought, the louder it rings in my consciousness. I steer my boat to chase after an endless feed of bewitching sounds.

This is why meditation feels like an impossible task to me. Imagine Ulysses locking himself to the mast whilst keeping the key in his back pocket. Whenever I try and sit down, be still, and stop thinking, I immediately start scheming of ways to break free. Every fiber of my ego starts firing in overdrive, telling me why I shouldn’t be doing this and proposing a whole host of *better* uses of my time.

I say I struggle with meditation because more often than not, I listen. I stop meditating to follow after some whim of my ego, some temporary dopamine rush that I think could be more gratifying.

Without fail, however, when I chase fleeting passions I am left disappointed. They reveal themselves to be nothing more than *moldering skeletons of men*, false promises for mental peace.

I go back to that little corner of my room because each time, I allow myself to listen to the songs a little bit longer before running after one. The practice is teaching me, ever so slowly, to relax into the mast I’ve tied myself to instead of fighting against it. I allow myself to hear the music without trying to grasp it.

> “Watch your mind all day long. Watch what it does. Don’t judge it, don’t try to control it… Meditation is just watching your own thoughts like you would watch anything else in the outside world.”
> — Naval Ravikant, The Joe Rogan Experience #1309

This is why I meditate. It is not so I can fuel some ego of being a *meditator*. It is not because I think I should. It is not even because I want to.

I do it because the practice offers me a little bit of grace every second I can remember to listen to the Sirens instead of chasing them. The tension in my shoulders drops, my awareness of the present moment deepens, and the songs I hear become richer. It all just starts happening of its own accord. It all arises mutually.

I like to imagine Ulysses free of any bondage, sitting at the bow of his ship with a grin on his face. He cherishes their enchanting songs, perfectly content as his ship sails past. There is nothing to do, no muse to chase. He just listens.

Imagining this, I find a little peace in my own mind.

*Namaste* :pray:

~ Reed

<hr>

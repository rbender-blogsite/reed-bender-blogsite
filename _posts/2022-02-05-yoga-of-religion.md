---
title: "The Yoga of Religion"
layout: post
date: 2022-02-05 12:00
image: /assets/images/markdown.jpg
headerImage: false
tag:
- yoga
- theology
star: true
category: blog
author: reedbender
description: Are yoga and religion two sides of the same coin?
---

<p style="text-align: center;"><i>Are yoga and religion two sides of the same coin?</i></p>


![Photo from [Unsplash](https://unsplash.com/photos/Pt9JWmvHbGw)](https://cdn-images-1.medium.com/max/11474/1*dSAyGzvMUiUGm7bnvRIniA.jpeg)*Photo from [Unsplash](https://unsplash.com/photos/Pt9JWmvHbGw)*

<hr>

> “In the course of the centuries the West will produce its own yoga, and it will be on the basis laid down by Christianity.”
> — Carl Jung, The Psychology of Kundalini Yoga

## Re-*ligō*

I recently dove down the rabbit hole of trying to understand the root of our word *religion*. This word, which has become so loaded with pretext in our culture, surely has a deeper origin.

*Religion* is derived from the latin *ligō, *meaning* *“to bandage, to wrap around, or to unite.” In latin’s predecessor, the Proto-Indo-European tongue, we can find the term *leyǵ-, *implying “to tie together.” The English words ligament, league, liaison, and ligate all arise from this common lineage.

*Re-ligion*, then, can be understood to mean “to re-ligament.” To tie back together something which has been divided. To re-unite.
> “Come to me, all you who are weary and burdened, and I will give you rest. Take my **yoke** upon you and learn from me, for I am gentle and humble in heart, and you will find rest for your souls. For my **yoke** is easy and my burden is light.”
> — Matthew 11:28–30

The Greek word for *yoke* (the book of Matthew was originally written in Greek) is *zygós: “*a wooden bar placed over the neck of a pair of animals so they can pull together; (figuratively) what *unites *(*joins*) two people to move (work) together as *one” ([*Source](https://biblehub.com/greek/2218.htm)*).*

A yoke is what was used to tie together two oxen at the neck so they could join forces and pull a heavy plow. Similarly, a yoke is “a frame fitted to a person’s shoulders to carry a load in two equal portions” ([Mirriam-Webster](https://www.merriam-webster.com/dictionary/yoke)). This word *yoke*, then, is implying an act of reconciling two opposites. A ligation of two independent forces so they can begin working together as a single harmony. When a yoke is placed on your shoulders, if the weight of the water on one side is heavier than the other it will cause you to topple over. The goal is to balance the weight evenly at the fulcrum, that center-point where you stand.

![On the left, a balance scale ([Source](https://commons.wikimedia.org/wiki/File:Brass_scales_with_cupped_trays.png)). On the right, a woman carrying a balanced yoke ([Source](https://upload.wikimedia.org/wikipedia/commons/4/46/Saint_Petersburg_woman_carrying_buckets_of_water%2C_near_Leningrad_%281%29.jpg)).](https://cdn-images-1.medium.com/max/2000/1*xwKyz4KxulouREzbOwmEvw.png)*On the left, a balance scale ([Source](https://commons.wikimedia.org/wiki/File:Brass_scales_with_cupped_trays.png)). On the right, a woman carrying a balanced yoke ([Source](https://upload.wikimedia.org/wikipedia/commons/4/46/Saint_Petersburg_woman_carrying_buckets_of_water%2C_near_Leningrad_%281%29.jpg)).*

Returning to Matthew, what is this *yoke* which Jesus is referring to? What is this balance Christ calls for us to take upon ourselves and learn from? What two things must be *re-ligamented*?

## Yoga
> ‘The word *yoga* is derived from the verbal root yuj (“to yoke” or “to harness”)… The Sanskrit term *yoga* was [defined] as the “union” between the lower or embodied self and the transcendental Self (atman).’
> — Georg Feuerstein, The Deeper Dimension of Yoga

The practice of yoga was developed in India thousands of years ago, built on top of their symbolic structure of Hindu cosmology. Yoga is often misunderstood as being either a religion in its own right, or more often in the West we consider it to be just a fashionable form of exercise. In reality, it is something entirely unique to us.

Yoga is an incredibly intricate practice, designed to purify the mind and body of the practitioner so that we might perceive more subtle energies within our bodies. There are legends of yogis who could consciously control blood flow, stop breathing entirely for hours on end while holding an exhale, or fast for years without consuming a single calorie of food (Paramhansa Yogananda’s* Autobiography of a Yogi *will blow your mind!). If you are like me and believe this sounds a little incredulous, there is scientific evidence now that experienced yogis can consciously control their heart rate and can even consciously dissociate from bodily pain (Dr. Robert Becker’s *The Electric Body* is another fascinating read).

Yoga is a science of the body. Dedicated practitioners in India seem to have developed this science centuries ago, exploring the impact of highly specific body movements, extreme breathing practices, and deep meditation on the yogi’s subjective experience. For centuries, yoga has been a science which instructs people how to perceive these subtle frequencies of energy around us.
> “If you can simply get to that subtle vibration, you will see and feel that the whole universe is composed of subtle vibrations… Beyond the vibrations of matter in its gross and subtle aspects, beyond motion there is but One.”
> — Swami Vivekananda, Raja Yoga

After you read this paragraph, for just 10 seconds, close your eyes and let your body become completely still. It doesn’t matter where you are. Feel your left ear. Focus on it, become aware of it, and just observe every little tingle and itch that arises. Feel the skin. Feel under the skin. Imagine blood flowing around the skull and into every crevice of the ear. Ask yourself, *what else might be there that I don’t feel yet*. Allow the left side of your face to answer you with a new sensation. Give it a try now for just 10 seconds.

This process of becoming intently aware of the feelings inside and around the body is the practice of yoga. Breathwork, *asanas* (the postures we associate with yoga), ethical morals, and meditation are the tools of the yogi to perceive these sensations which our mind normally filters out as background.
> “[This] is the ultimate goal of yoga: union with our highest Self. The pure, joyful bliss that we experience when we are able to withdraw from the distracting, outward-going senses and into the silence of the heart is truly what the effort is all about.”
> — Swami Sivananda, quoted in “Letters From the Yoga Masters”

In the cosmology of Hinduism, The *Brahman* is the Universal spirit of consciousness. The all-pervading, omnipresent Source which permeates all things. The *Brahman* is then fractal-ed out into an infinite number of pieces, each of which contains the spirit of the totality inside it. This individual spirit is the *Atman*. It is believed that every human has this spark of the *Atman* inside of us, hiding behind the “lower or embodied self.” This is the *ego*, or the identity which we give ourselves. According to Hinduism, the spirit of *God* is hiding in us all.

It is important to distinguish between the science of yoga and the religion of Hinduism.

The practice of yoga is known to lead the practitioner into extraordinary experiences. The subjective reports of what meditators or yogis can perceive in their inner-worlds are completely unfathomable from the perspective of our every day waking consciousness (*Autobiography of a Yogi *makes this clear). The goal of yoga, then, is to move beyond our embodied self so that we might *re-ligament* with the *Atman*, that piece of divine intelligence which dwells within us all.

Religion, then, can be understood as the symbolic meaning of these encounters. If these extraordinary states of consciousness are possible, then it would be necessary for the people who perceive them to have some way of explaining what they had been subjected to. For the ancient yogis of India, Hinduism was the symbolic structure they had. The variety of Hindu deities and myths would have been a narrative structure for yogis to use while mapping these inner-realms.

Yoga and religion are playing two very different roles. One is the science of how a person can achieve altered states of consciousness. The other is the sense-making symbolism which allows the yogi to not be completely dumbfounded in response.

Yoga is a practice to achieve transcendental consciousness. Religion is the collection of stories from people who have done it in the past.

## Reconciliation
> “The Word became flesh and made his dwelling **within** us. We have seen his glory, the glory of the one and only Son, who came from the Father, full of grace and truth.”
> — John 1:14

I believe that one of the reasons yoga has been so misunderstood by us in the West is because we cannot understand the symbolic guideposts of Hindu cosmology. In our culture built out of a Christian faith, we have no emotional connection to the Hindu legends or the *Brahman*. It does not speak to us here, so we overlook it.

But the practice of yoga is not inextricably tied to the religions of India. If we allow ourselves to adopt the yogic science, there is no reason we cannot interpret it through our Christian symbolism. Upon closer inspection, there is more in common than we might think.

The cosmology of John is beautifully outlined in the first chapter of his book. The Greek conception of the *Logos *is the divine mind, the deepest meaning for life. The *Logos* is the *Word* that became flesh, the Universal spirit of *God *which pervades all of creation.

The *Logos* of the Greeks is the same conception as the *Brahman* of the Hindus. It is the Universal Source which gives life to all things.

This passage of John 1:14, as printed in your typical King James or New International Versions of the Bible, will read like this: *The Word became flesh and made his dwelling **among** us.* While this difference may seem trivial, a slight change in phrasing results in a radical change in meaning.

In the context of Christian theology, this passage is typically understood as claiming that *God* became man in the person of Jesus exclusively, who then dwelt *among* us here on Earth. Jesus, being the one and only Son of God, is unlike the rest of us in his simultaneous divinity and humanity.

The Greek, however, is more mysterious than this orthodox understanding. The Greek word in question is *ἐν. *This Greek preposition appears 2801 times in the King James Version of the bible, and 1902 of those times it is translated as *in* or *within*. Only 117 times is *ἐν *translated as *among *(other possible translations include *on*, *through*, or *by*, among others*) ([*Source](https://bible.knowing-jesus.com/strongs/G1722)*).*

My interpretation is that John was demonstrating how the spirit of the *Logos *became flesh and made its dwelling inside of each and every one of us. **The** **Brahman became the Atman and made its dwelling within us all.** We are all one and the same with the Son (*Atman*), who came from the Father (*Brahman*).

Another point to make about this passage is that the Greek word for son, *hyiós, *does not imply direct lineage as in a human birth from a father. Rather, it implies that the son is of the same substance as the Father. The son of an ape will always have the inherent nature of *ape-ness*. The Son of God, in its original Greek meaning, would have conveyed the idea that Jesus was of the same inherent nature as *God* (this is the Christian doctrine of [homoousios](https://www.britannica.com/topic/homoousios)) ([Source](https://biblehub.com/greek/5207.htm)). Jesus’ claim to be descended from *God* was equivalent to his incredible claim that “I and the Father are one.”

Considering this mystical interpretation of John, I would like to highlight one other teaching of the Christ which has the power to radically change our conception of his message.
> ‘“I and the Father are one.”
> Again his Jewish opponents picked up stones to stone him, but Jesus said to them, “I have shown you many good works from the Father. For which of these do you stone me?”
> “We are not stoning you for any good work,” they replied, “but for blasphemy, because you, a mere man, claim to be God.”
> Jesus answered them, “Is it not written in your Law, ‘I have said you are “gods?”
> If he called them ‘gods,’ to whom the word of God came — and Scripture cannot be set aside — what about the one whom the Father set apart as his very own and sent into the world? Why then do you accuse me of blasphemy because I said, “I am the son of God”?’
> — John 10:30–36

Here, the Pharisees are accusing Jesus of blasphemy because he is claiming to be both *God* and man. In response, he quotes their own scripture back to them, as he often did. He throws Psalm 82 at the Pharisees in response to their accusations, which says “Ye are gods; and all of you are children of the most High.”

In essence, Jesus is saying “of course I am claiming to be *God*! And your own scripture says that all of you are gods too!”

Jesus proceeds to say ‘why then do you accuse me of blasphemy because I said, “I am the son of *God*?”’ But, again, this is not a perfectly faithful interpretation. In the Greek language, there is not a variety of articles such as *the* or *a*. The Greek only has one ([Source](http://www.ibiblio.org/koine/greek/lessons/noun2dcl.html)). Nowhere in this passage does Jesus claim to be the only Son of God. A literal interpretation of this passage would be read like Yoda, ending with “Son of *God* I am.” Or, more accurately:
> # “*Of the same substance as God I am*.”

Jesus’ point seems to be that the Pharisees’ own scripture says they are all divine. So how can they persecute him for stating the obvious?
> ‘So the Christians said, “Okay, okay, Jesus of Nazareth was the Son of God. But let it stop right there! Nobody else.” So what happened was that Jesus was pedestalized. He was put in a position that was safely upstairs, so that his troubles and experience of cosmic consciousness would not come and cause other people to be a nuisance… Here is the revelation of God in Christ, in Jesus, and we are supposed to follow his life and example without having the unique advantage of being the boss’s son.’
> — Alan Watts ([Source](https://alanwatts.org/1-3-3-jesus-his-religion-pt-2/))

This mystical interpretation implies that Christ had realized a divine *yoga* within himself and sought to teach it to the rest of the world. He claimed no special status, beyond being a uniquely conscious begotten Son of *God. *Despite his fully realized divine nature, Jesus did not believe that this equality with God was something to be used to his advantage (Philippians 2:6). Christ instead spoke a great deal to convey that he was, in fact, still fully man. Just like us in every way. His message was for us all to re-connect with that divine spark within ourselves that is of the same substance as the *Logos*.

The aim of all spiritual practice is to *re-ligament* the human with the divine. To make these two realms merge as One inside of every human, despite the utter paradox this presents. I believe that Christ sought to teach us this lesson, spending his life utterly devoid of ego. Fully yoked to the *Brahman*, Christ pointed us all to find that Truth within ourselves.

We can take the science of yoga, designed to achieve this divine union, and make sense of it here in the West through our lens of Christ Jesus.
> ‘Brahma thought for a long time and then said, “Here is what we will do. We will hide their divinity deep in the center of their own being, for humans will never think to look for it there.”’
> — Hindu legend ([Source](http://www.naute.com/stories/hideout.phtml))

<br/>

*Namaste* :pray:

~ Reed

<hr>

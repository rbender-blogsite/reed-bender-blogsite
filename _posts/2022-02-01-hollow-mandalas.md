---
title: "Hollow Mandalas"
layout: post
date: 2022-02-01 12:00
image: /assets/images/markdown.jpg
headerImage: false
tag:
- poems
star: true
category: blog
author: reedbender
description: 
---

<hr>

In a world so clearly broken, can the center still be found?

Or does it lie defeated, beaten from the crown?

That end I push outside me

that treasure long forgotten

two thousand years and now I’m free

my crown of thorns has fallen.

So now I sit and wait for this kingdom to restore

the life of bliss I feel I’m due, I know there must be more.

A cosmic dance, transcendent gaze, whatever may appear

I don’t know what I’m after, just anything but here.

Samsara spins around me, a witness to this cruel unfolding

the deeper down the hole I go the more I hear the scolding.

Chasing peace and finding pain, yet here I find what’s true

Always there before me, creation sings in two.

<br/>

*Namaste* :pray:

~ Reed

<hr>

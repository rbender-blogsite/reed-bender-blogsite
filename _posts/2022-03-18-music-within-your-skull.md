---
title: "The Music Within Your Skull"
layout: post
date: 2022-03-18 12:00
image: /assets/images/markdown.jpg
headerImage: false
tag:
- binaural
- neuroscience
star: true
category: blog
author: reedbender
description: An internal electric harmony
---


<p style="text-align: center;"><i>An internal electric harmony</i></p>


![Photo via [Pixabay](https://www.google.com/url?sa=i&url=https%3A%2F%2Fpixabay.com%2Fillustrations%2Fbrain-waves-consciousness-4372153%2F&psig=AOvVaw0euG9n5sqeiCLUWibdXbJR&ust=1645219303029000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCNCS_NTVh_YCFQAAAAAdAAAAABAD)](https://cdn-images-1.medium.com/max/2560/1*Tc5gWb65blnvoRiEfEFBNg.jpeg)*Photo via [Pixabay](https://www.google.com/url?sa=i&url=https%3A%2F%2Fpixabay.com%2Fillustrations%2Fbrain-waves-consciousness-4372153%2F&psig=AOvVaw0euG9n5sqeiCLUWibdXbJR&ust=1645219303029000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCNCS_NTVh_YCFQAAAAAdAAAAABAD)*

<hr>

> “The Brain is waking and with it the mind is returning. It is as if the Milky Way entered upon some cosmic dance. Swiftly the head-mass becomes an enchanted loom, where millions of flushing shuttles weave a dissolving pattern, though never abiding one: A shifting harmony of subtle patterns.”
> — Sir Charles Sherrington, Man on his Nature (1940)

Consider, for a moment, what it feels like when you first wake up on a sunny Saturday morning. As you roll over, fragments of a forgotten dream fill your mind. There is no rush to run off to work or chug coffee. As you linger in that liminal space between sleep and wakefulness, your mind floats in pure imagination. It might even feel like you haven’t fully left the dream. You play it over in your head, curiously pondering what might have inspired such a strange plot.

Now to juxtapose this, imagine it is 3:48 on a Monday afternoon and you are racing to meet a 4:00 deadline. Three cups of coffee deep, you power through the task despite the underlying anxiety. Intently focussed on the task at hand, your attention is directed out into the world.

It only takes a tad of self-reflection to recognize these two mental modes within ourselves. What emotions would you feel in either of those scenarios? In my head, these experiences could not be more drastically different. The inner-peace of Saturday morning is introspective and calm, while the rush of Monday afternoon is extrospective and stressful.

This leads to a question: *How does the brain dictate these perceptions?*

When thinking about the brain, I am guilty of focusing on the chemistry alone. Molecules such as dopamine, serotonin, and adrenaline come to mind when thinking about emotions. When we are content with what we have, serotonin spikes. When we are motivated towards a goal or receive a gratifying reward, dopamine is released. When we are actively engaged or stressed, adrenaline courses around the brain.

Many of these chemicals, however, act as what are called *neuromodulators*. The chemicals, in-and-of themselves, do not directly dictate our experience. Rather, they instruct certain neurons to fire and other neurons to quiet down ([Source](https://youtu.be/H-XfCl-HpRM)). In this act of coordinating which neurons are active and which are quiescent, the brain shifts between patterns of behavior. There is an old saying in neuroscience:
> # “Neurons that fire together wire together.”

Rather than a tangle of cells, I like to think of the brain as an orchestra. A full symphony orchestra contains four unique sections (strings, woodwind, brass, percussion). Similarly, your brain is made up of unique lobes (frontal, parietal, occipital, temporal).

Each of the individual neurons of the brain can be thought of as a sole instrument in an orchestra (albeit an orchestra made up of nearly 100 billion instruments). Somehow, they all work together to produce *you*. It isn’t the neurons that make this miracle reality; it is the network of connections between them.

Two instruments can be very tightly linked in a work of music. Even if one is a violin and the other is a trombone, these two disparate tones can sing together. If the musicians begin to harmonize, they will be more likely to repeat this pattern. As they acclimate to singing together, the likelihood that they will continue to harmonize in the future increases.

**The brain repeats the patterns it practices.**

Many of the chemicals in our brains, then, act like composers. Dopamine might instruct the clarinet to be silent while the cello picks up tempo. Serotonin might then come in and tell the piano and the flute to sync together and up the volume. As they do this repeatedly, those neurons will *wire together.* The cells will be linked to one another and those patterns of activity will be repeated in the future.

Imaging the brain in this way, a picture of neurons interconnected into a vast web begins to emerge. When neurons fire, they send an electrical signal to a select set of other neurons, which then send a signal of their own to other neurons across the network. This is how neurons wire together. It is this infinitely complex dance that makes you **aware in the here and now**.
> “The human brain has 100 billion neurons, each neuron connected to 10 thousand other neurons. Sitting on your shoulders is the most complicated object in the known universe.”
> — Michio Kaku, American theoretical physicist

100,000,000,000 neurons, each linked with 10,000 others. That equates to one hundred trillion (**10¹⁴**) electrical bridges inside your skull. Each of those bridges is a note woven into the symphony of your conscious awareness.

We now possess the capability to listen to the brain as these electrical bridges fire and wire together. These patterns are known as *brainwaves. *The frequency with which those neurons pulse can be observed by connecting electrodes to a person’s scalp and measuring the subtle changes in electrical activity across the skull.

Generally speaking, faster electrical pulses across the brain correspond to more extroverted awareness while slower brainwaves relate with deep, meditative attention and sleep.

Returning to the question of how this harmony dictates your perception, we can think about enjoying different modalities of music.

On the one end of this spectrum is AC/DC’s *Thunderstruck*. The fast tempo and wild guitar tend to have the effect of making us more energetic, more active, and certainly more excited. Adrenaline spikes as we feel the urge to tap our foot and rage with the beat.

This can be correlated to the the ***Beta*** brainwave pattern. Oscillating between 12–35 Hz, these are the rapid-strobe brainwave patterns observed during high stress moments of extroverted attention. These are the fastest brainwaves of our normal waking consciousness ([Source](https://www.sciencedirect.com/topics/agricultural-and-biological-sciences/brain-waves)).*

* *Note that there is a faster brainwave pattern, known as *Gamma, *which oscillates at 40 Hz and above. These states, however, appear to function in a wholly different manner and bring the brain into coherent patterns to aid in memory, learning, general cognition, and all-together stranger states of awareness. I will return to these patterns in future articles.

Your brain is entrained to the Beta pattern whenever you are engaged in outward-focussed, engaging, or stressful activities. In these states, your mind is active and busy. On the Monday afternoon as you race to meet a deadline, your brain is most likely functioning in this state.

The problem with Beta brainwave patterns is that they can lead to anxiety and stress if sustained for prolonged periods of time. Our modern life, however, keeps us constantly engaged and pulls our attention in a thousand different directions at all times. When we say that we need to “calm down,” we are really saying that we need to turn down the frequency of our brainwaves to a slower drumbeat.

As we achieve this relaxing, we may pass into the ***Alpha*** brainwave state. Here, your brain is firing between 8–12 cycles per second. Your awareness will become more introspective, creative, and peaceful as you start to engage with more self-reflective thoughts.

If, during that Monday afternoon crunch, you take a five minute break to step outside and take a walk around the block to clear your mind, you would effectively bring your brainwaves down into the Alpha state ([Source](https://www.scientificamerican.com/article/what-is-the-function-of-t-1997-12-22/#:~:text=Alpha%20brainwaves%20are%20slower%2C%20and,usually%20in%20an%20alpha%20state.)).

If you relax farther, then you might dip down into the ***Theta*** brainwave state. Oscillating between 4–8 Hz, this is the state of mind often associated with meditation or contemplation. You are surely familiar with this state, irrespective of any experience with meditation. This is the state of mind where creative inspiration penetrates your awareness. It is as if we change the song in our head from *Thunderstruck *and now begin listening to the vibe of Louis Armstrong’s *What a Wonderful World*.

You have likely experienced Theta brainwave patterns if you’ve ever had a new idea while in the shower or on the commute to work. When you don’t actively need to think about the activity at hand, your mind begins to wander and daydream. This is also the pattern of neural oscillations that we imagined on a Saturday morning while you lie in the liminal space between dreaming and wakefulness. It is here that creativity arises ([Source](https://gohighbrow.com/brain-waves/#:~:text=Theta%20waves%20are%20lower%20frequency,them%20to%20rest%20%5B1%5D.)).

Finally, as you relax even further you will fall asleep. The deepest stage of your sleep, where dreams are absent and the brain is essentially in hibernation, is the ***Delta*** state. These brainwaves vibrate between 0.5–4 Hz, as your brain is essentially quiet. It is in this stage of deep sleep that your brain rests and resets for the day to come.

I believe that when we understand the brain in this way, we open up a whole new toolbox for taking control of our mood and our perceptions.

If we think of the brain as just chemistry, we are at risk of believing it is something outside of our conscious control. We resort to taking pills and looking for external stimuli to change the way we feel and respond to the world.

But when we begin to think of the brain as a musical harmony, we suddenly have the ability to become the master conductor of the symphony.

Some of the most powerful tools for controlling our own mind are **binaural beats**.

Science has discovered a neat quirk of our brains: it is capable of what is now called the *Frequency Following Response* (FFR) ([Source](https://www.nature.com/articles/s41467-019-13003-w#:~:text=Frequency%2Dfollowing%20responses%20(FFRs),sound%2C%20recorded%20from%20the%20scalp.)). When you listen to a particular frequency of sound, your brain will fall into electrical coherence with that frequency.

So, when you listen to *Thunderstruck*, your brain actually becomes entrained to the rapid pace of the guitar. Your neural network will follow that frequency. Likewise, when you listen to *What a Wonderful World*, your brain starts to electrically fire and wire at that same slow melody. This is largely why music has such a powerful impact on our mood.

One issue here, however, is that we cannot take advantage of this effect to entrain to the slower brainwave patterns of Alpha, Theta, or Delta with single tones of sound. The human ear is not capable of perceiving sounds below 20 Hz, so we cannot actually hear these slower frequency ranges ([Source](https://www.ncbi.nlm.nih.gov/books/NBK10924/#:~:text=Humans%20can%20detect%20sounds%20in,to%2015%E2%80%9317%20kHz.))).

This is where binaural beats become useful. The brain is capable of an auditory illusion where if two slightly different tones are played in each ear, the brain will perceive the difference between them and create its own sound. If you wear stereo headphones and play 440 Hz in one ear while listening to 444 Hz in the other, your brain will actually create a third tone of 4 Hz that plays within your skull. Then, due to the FFR, the brain will become entrained to this new 4 Hz tone and begin to resonate at that frequency.

Using this principle, we can effectively tune our brainwaves like you would tune an instrument. All that is required is YouTube and a set of headphones. If you are struggling to fall asleep, you could try listening to a 3.2 Hz binaural beat while allowing your body to relax.

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/xsfyb1pStdw" frameborder="0" allowfullscreen></iframe></center>

Likewise, if you are looking to deepen your meditation practice, you could try listening to a 7 Hz binaural beat for 10 minutes while allowing your mind to wander into uncharted creative musings.

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/4D9ZF9mB2LQ" frameborder="0" allowfullscreen></iframe></center>

These might sound strange at first, but if you listen to these sounds with a decent pair of studio headphones then you might eventually start to recognize a kind of wobble in the sound that seems to oscillate between your ears. This is simply the sensation of your brain generating its own third tone as your perception begins to entrain to that sound.

Modern research is beginning to show a wealth of positive psychological and physical effects from the use of binaural beats in this way.

Evidence suggests that binaural beats can be an effective method for [controlling anxiety](https://associationofanaesthetists-publications.onlinelibrary.wiley.com/doi/full/10.1111/j.1365-2044.2005.04287.x), [improving sleep quality](https://pubmed.ncbi.nlm.nih.gov/23862643/), [aiding creativity](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3827550/), and even stimulating [learning and memory](https://link.springer.com/article/10.1007/s00426-015-0727-0).

A great deal more research is necessary to fully understand the effect of binaural beats of these various aspects of our life, but it is clear that they are a tool which can be used to make controlling your mental state a much more conscious process. Ultimately, they offer a method of tuning the dial of your awareness into particular points along the spectrum between extroverted activity and introverted contemplation.

The best way to understand the impact of these tones, and experimentally understand the electrical harmony that is your mind, is to simply experiment for yourself. As we begin to move away from our conception of the body as chemistry, the mystery of our electrical nature will become more apparent to us on an experiential level. It is here that we will discover the true capabilities of the human machine.
[**Our Superconducting Consciousness**
*A synthesis of neurophysiology, alchemy, and yoga into a theory of transcendental experience*medium.com](https://medium.com/@reedbender/our-superconducing-consciousness-26c9a82072fc)


<br/>

*Namaste* :pray:

~ Reed

<hr>
